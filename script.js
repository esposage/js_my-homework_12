// Теоретичні питання
// 1. Чому для роботи з input не рекомендується використовувати клавіатуру ?
//   Подія в input спрацьовує при будь - якій зміні, навіть якщо ці зміни не пов"язані з введенням  з клавіатури, 
// а саме введення тексту через розпізнавання голосу, вставка тексту за допомогою миші.
// Але подія в input  не спрацює при вводі з клавіатури, якщо ці зміни не будуть пов"язані 
// з натисканням, наприклад, клавіш стрілок вліво / вправо під час введення.


const buttons = document.querySelectorAll('.btn');
let currentBlueButton = null;

document.addEventListener('keydown', event => {
  const key = event.key.toUpperCase();
  if (currentBlueButton) {
    currentBlueButton.classList.remove('blue');
  }
  buttons.forEach(button => {
    if (button.innerText === key) {
      button.classList.add('blue');
      currentBlueButton = button;
    }
  });
  if (key === 'ENTER') {
    buttons[0].classList.add('blue');
    currentBlueButton = buttons[0];
  }
});
